# TASK MANAGER

## DEVELOPER

**NAME**: Alexander Kolobov

**E-MAIL**: akolobov@t1-consulting.ru

**URL**: https://t1-consulting.ru/

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i5

**RAM**: 16Gb

**HDD**: 500Gb

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
