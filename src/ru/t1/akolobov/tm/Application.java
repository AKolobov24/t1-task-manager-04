package ru.t1.akolobov.tm;

import static ru.t1.akolobov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null || args.length == 0) {
            displayArgumentError();
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_HELP:
                displayHelp();
                break;
            default:
                displayArgumentError();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Display application version. \n", CMD_VERSION);
        System.out.printf("%s - Display developer info. \n", CMD_ABOUT);
        System.out.printf("%s - Display list of terminal commands.\n", CMD_HELP);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer: %s \n", "Alexander Kolobov");
        System.out.printf("e-mail: %s\n", "akolobov@t1-consulting.ru");
    }

    private static void displayArgumentError() {
        System.err.println("Error! This argument is not supported. \nUse 'help' to display available arguments.");
    }

}
